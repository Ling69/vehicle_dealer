DROP TABLE IF EXISTS Vehicle;
CREATE TABLE Vehicle (
    id INT PRIMARY KEY AUTO_INCREMENT,
    type ENUM ('Car', 'Moto', 'Bycycle') NOT NULL,
    brand VARCHAR(64) NOT NULL,
    color VARCHAR(64),
    price_of_purchase FLOAT NOT NULL,
    price_of_sale FLOAT NOT NULL,
    date_of_acquisition DATE
    
    );

INSERT INTO Vehicle (type, brand, color, price_of_purchase, price_of_sale, date_of_acquisition) VALUES
("Car","Jaguar", "Azure", "3", "1", "2021-12-25");

DROP TABLE IF EXISTS Car;
CREATE TABLE Car (
    id INT PRIMARY KEY AUTO_INCREMENT,
    type ENUM ('Break', 'Cut', 'SUV') NOT NULL,
    pourchasePrice FLOAT NOT NULL,
    salePrice FLOAT NOT NULL,
    gateNumber INT,
    power INT
    
    );

INSERT INTO Car (type, pourchasePrice, salePrice, gateNumber, power) VALUES
("Break","50,000", )