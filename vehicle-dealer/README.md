# Vehicle Dealer Front

## Contexte du projet

Our compay was hired by BestDeals Ltd which based in the United States.

They want to launch an online platform for the resale of used vehicles that they buy at auction.

Dans un premier temps, ils vont vendre des vélos, des motos, des scooters, et des voitures. Ils se réservent la possibilité à l'avenir de vendre des camions, des bateaux, des drones, des avions, la seule limite étant leur imagination.

My manager has decided to entrust me with the development of the prototype of the vehicle registre  form, which will be accessible in the site's administration panel.

I have to to present a working prototype to the client. I have to create three web pages:

    a page with the list of vehicles registered in the database
    a page with a vehicle file
    most importantly, a form for entering a new vehicle

​
Le formulaire doit permettre de choisir le type de véhicule, puis de saisir les informations correspondantes (marque, couleur, date d'achat, prix d'achat, prix de revente HT, et toutes les informations classiques pour chaque type de véhicule).

​

Les trois pages doivent être fonctionnelles (pas forcément jolies). Le formulaire doit permettre d'ajouter un vélo, une moto, ou une voiture au choix. Les éléments communs aux trois véhicules, ainsi que 2 ou 3 éléments spécifiques à chacun d'entre eux doivent être présents.

Par exemple : on doit pouvoir saisir la marque, la date d'acquisition, la couleur, et le prix de vente de chacun des véhicules. Pour les vélos, on va ajouter le type de vélo (route, VTT, etc), le nombre de plateaux, s'il y a un marquage anti-vol ou pas. Pour les véhicules à moteur, le kilométrage et la date de première mise en circulation. Pour les voitures, on va indiquer le type, le nombre de portes, si la boîte de vitesse est mécanique ou automatique, et la puissance en chevaux. Pour les motos, la cylindrée, la puissance, et le type.

Attention, le formulaire ne doit suggérer que les valeurs cohérentes avec le véhicule choisi.

​

Le client étant une société américaine, votre code et l'interface Front doivent être rédigés en anglais. Pour le Readme, l'anglais est un bonus, mais vous pouvez le rédiger en français si vous estimez que vos explications seront plus claires en français.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
