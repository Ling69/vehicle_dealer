package co.simplon.alt3.vehicledealer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleDealerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleDealerApplication.class, args);
	}

}
