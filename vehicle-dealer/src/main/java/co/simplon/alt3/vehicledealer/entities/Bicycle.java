package co.simplon.alt3.vehicledealer.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Bicycle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    private BicycleTypeEnum type;
    private int traysNumber;

    public Bicycle() {
    }
    public Bicycle(BicycleTypeEnum type, int traysNumber) {
        this.type = type;
        this.traysNumber = traysNumber;
    }
    public Bicycle(int id, BicycleTypeEnum type, int traysNumber) {
        this.id = id;
        this.type = type;
        this.traysNumber = traysNumber;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public BicycleTypeEnum getType() {
        return type;
    }
    public void setType(BicycleTypeEnum type) {
        this.type = type;
    }
    public int getTraysNumber() {
        return traysNumber;
    }
    public void setTraysNumber(int traysNumber) {
        this.traysNumber = traysNumber;
    }

}
