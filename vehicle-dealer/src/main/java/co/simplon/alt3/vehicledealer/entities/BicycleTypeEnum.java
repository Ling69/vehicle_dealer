package co.simplon.alt3.vehicledealer.entities;

public enum BicycleTypeEnum {
    Route,
    Mountain,
    Touring;
}
