package co.simplon.alt3.vehicledealer.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    private CarTypeEnum type;
    
    private float purchasePrice;
    private float salePrice;
    private int gateNumber;
    private int power;
    
    public Car() {
    }

    public Car(CarTypeEnum type, float purchasePrice, float salePrice, int gateNumber, int power) {
        this.type = type;
        this.purchasePrice = purchasePrice;
        this.salePrice = salePrice;
        this.gateNumber = gateNumber;
        this.power = power;
    }

    public Car(int id, CarTypeEnum type, float purchasePrice, float salePrice, int gateNumber, int power) {
        this.id = id;
        this.type = type;
        this.purchasePrice = purchasePrice;
        this.salePrice = salePrice;
        this.gateNumber = gateNumber;
        this.power = power;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarTypeEnum getType() {
        return type;
    }

    public void setType(CarTypeEnum type) {
        this.type = type;
    }

    public float getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(float purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(float salePrice) {
        this.salePrice = salePrice;
    }

    public int getGateNumber() {
        return gateNumber;
    }

    public void setGateNumber(int gateNumber) {
        this.gateNumber = gateNumber;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
    
}
