package co.simplon.alt3.vehicledealer.entities;

public enum CarTypeEnum {
    Break,
    Cut,
    SUV;
}
