package co.simplon.alt3.vehicledealer.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Motorcycle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Enumerated(EnumType.STRING)
    private MotorcycleTypeEnum type;
    
    private int cylindre;
    private int power;
    
    public Motorcycle() {
    }

    public Motorcycle(MotorcycleTypeEnum type, int cylindre, int power) {
        this.type = type;
        this.cylindre = cylindre;
        this.power = power;
    }

    public Motorcycle(int id, MotorcycleTypeEnum type, int cylindre, int power) {
        this.id = id;
        this.type = type;
        this.cylindre = cylindre;
        this.power = power;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MotorcycleTypeEnum getType() {
        return type;
    }

    public void setType(MotorcycleTypeEnum type) {
        this.type = type;
    }

    public int getCylindre() {
        return cylindre;
    }

    public void setCylindre(int cylindre) {
        this.cylindre = cylindre;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
    
    
}
