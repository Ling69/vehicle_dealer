package co.simplon.alt3.vehicledealer.entities;

public enum MotorcycleTypeEnum {
    
    Standard,
    Cruiser,
    Touring

}
