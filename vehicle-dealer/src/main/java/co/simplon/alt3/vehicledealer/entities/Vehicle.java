package co.simplon.alt3.vehicledealer.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    private VehicleTypeEnum type;

    private String brand;
    private String color;
    private float price_of_purchase;
    private float price_of_sale;

    @Column(nullable = false)
    private Date date_of_acquisition;

    public Vehicle() {
    }

    public Vehicle(VehicleTypeEnum type, String brand, String color, float price_of_purchase, float price_of_sale,
            Date date_of_acquisition) {
        this.type = type;
        this.brand = brand;
        this.color = color;
        this.price_of_purchase = price_of_purchase;
        this.price_of_sale = price_of_sale;
        this.date_of_acquisition = date_of_acquisition;
    }

    public Vehicle(int id, VehicleTypeEnum type, String brand, String color, float price_of_purchase,
            float price_of_sale, Date date_of_acquisition) {
        this.id = id;
        this.type = type;
        this.brand = brand;
        this.color = color;
        this.price_of_purchase = price_of_purchase;
        this.price_of_sale = price_of_sale;
        this.date_of_acquisition = date_of_acquisition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getPrice_of_purchase() {
        return price_of_purchase;
    }

    public void setPrice_of_purchase(float price_of_purchase) {
        this.price_of_purchase = price_of_purchase;
    }

    public float getPrice_of_sale() {
        return price_of_sale;
    }

    public void setPrice_of_sale(float price_of_sale) {
        this.price_of_sale = price_of_sale;
    }

    public Date getDate_of_acquisition() {
        return date_of_acquisition;
    }

    public void setDate_of_acquisition(Date date_of_acquisition) {
        this.date_of_acquisition = date_of_acquisition;
    }
    

    

}