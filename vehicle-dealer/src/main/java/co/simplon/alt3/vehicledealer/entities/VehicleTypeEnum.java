package co.simplon.alt3.vehicledealer.entities;

public enum VehicleTypeEnum {
        Car("Car"), 
        Moto("Moto"), 
        Bycycle("Bycycle");

private String value;

private VehicleTypeEnum(String value) {
        this.value = value;
};

public String getValue() {
        return value;
}

}

